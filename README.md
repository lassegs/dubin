# Dubin

![Screenshot](https://i.imgur.com/5kPSGBi.png)

## Description
A simple theme to stylize [revealjs](revealjs.com/) presentations in [the Research Council of Norway (Forskningsrådet)](//forskningsradet.no)'s brand, which aims to adhere to the [brand guide](//identitet.forskningsradet.no).

Developed on the 'White' theme, but should work on others too. Loads fonts, logo and colors. For quality of life, there's also two grid layout classes.
## Usage

Load the CSS file after Reveals theme files.

For instance:

```
    ...
    <link rel="stylesheet" href="https://unpkg.com/reveal.js@^4//dist/theme/white.css" id="theme">
    <link rel="stylesheet" href="https://glcdn.githack.com/lassegs/dubin/-/raw/main/dubin.css">
    </head>
    ...
```

or with pandoc

```
pandoc -t revealjs -s exampleinput.md -o exampleoutput.html --variable theme="white" --css "https://glcdn.githack.com/lassegs/dubin/-/raw/main/dubin.css"
```

To use grid layout, create a container div with class `.halfsplit` or `.thirdsplit`, and put content inside `.gridleft` and `.gridright`.

## Authors and acknowledgment
Lasse Gullvåg Sætre

## License
GPLv3
